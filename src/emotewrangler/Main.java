package emotewrangler;
import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.StandardCopyOption.*;
import javax.swing.JOptionPane;
/**
 *
 * @author Nathaniel
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            File cwd = new File("").getAbsoluteFile();
            File[] children = cwd.listFiles();
            File custom = new File("custom");
            custom.mkdir();
            File defaultJSON = new File("default.json");
            defaultJSON.createNewFile();
            File emotesTXT = new File("EMOTES.txt");
            emotesTXT.createNewFile();
            
            PrintWriter jsonpw = new PrintWriter(defaultJSON);
            PrintWriter txtpw = new PrintWriter(emotesTXT);
            
            int step = 2;
            String offs = JOptionPane.showInputDialog(null, "Please enter the lowest unicode code point to be used\nby these emotes. Each emote takes two unicode code points. \n(Note: E000-F8FF is designated as private use for UTF-8)", "E000");
            int id = Integer.parseInt(offs, 16);
            jsonpw.println("{");
            jsonpw.println("    \"providers\": [");
            
            int indx = 0;
            boolean noneyet = true;
            while(indx < children.length && noneyet){
                String filename = children[indx].getName();
                boolean valid = (filename.endsWith(".png") || filename.endsWith(".gif")) && !(filename.endsWith("_big.png") || filename.endsWith("_big.gif"));
                if(valid){
                    String acceptable = fix(filename);
                    File target;
                    
                    String big_acceptable = acceptable;
                    
                    //handle big version
                    String bigfilename = filename.substring(0, filename.length() - 4) + "_big" + filename.substring(filename.length() - 4);
                    File bigfile = new File(bigfilename);
                    if(bigfile.exists()){
                        big_acceptable = fix(bigfilename);
                        target = new File("custom/" + big_acceptable);
                        Files.copy(bigfile.getAbsoluteFile().toPath(), target.getAbsoluteFile().toPath(), REPLACE_EXISTING);
                    }
                    
                    
                    txtpw.print(filename.substring(0, filename.length() - 4) + "," + Integer.toHexString(id));
                    jsonpw.println("        {");
                    jsonpw.println("            \"type\": \"bitmap\",");
                    jsonpw.println("            \"file\": \"minecraft:custom/" + acceptable + "\",");
                    jsonpw.println("            \"ascent\": 8,");
                    jsonpw.println("            \"height\": 8,");
                    jsonpw.println("            \"chars\": [\"\\u" + Integer.toHexString(id) + "\"]");
                    jsonpw.println("        },");
                    jsonpw.println("        {");
                    jsonpw.println("            \"type\": \"bitmap\",");
                    jsonpw.println("            \"file\": \"minecraft:custom/" + big_acceptable + "\",");
                    jsonpw.println("            \"ascent\": 24,");
                    jsonpw.println("            \"height\": 24,");
                    jsonpw.println("            \"chars\": [\"\\u" + Integer.toHexString(id + 1) + "\"]");
                    jsonpw.print("        }");
                    
                    target = new File("custom/" + acceptable);
                    
                    Files.copy(children[indx].getAbsoluteFile().toPath(), target.getAbsoluteFile().toPath(), REPLACE_EXISTING);
                    
                    id += step;
                    noneyet = false;
                }
                
                indx ++;
            }
            for(int i = indx; i < children.length; i++){
                String filename = children[i].getName();
                boolean valid = (filename.endsWith(".png") || filename.endsWith(".gif")) && !(filename.endsWith("_big.png") || filename.endsWith("_big.gif"));
                if(valid){
                    String acceptable = fix(filename);
                    File target;
                    
                    String big_acceptable = acceptable;
                    
                    //handle big version
                    String bigfilename = filename.substring(0, filename.length() - 4) + "_big" + filename.substring(filename.length() - 4);
                    File bigfile = new File(bigfilename);
                    if(bigfile.exists()){
                        big_acceptable = fix(bigfilename);
                        target = new File("custom/" + big_acceptable);
                        Files.copy(bigfile.getAbsoluteFile().toPath(), target.getAbsoluteFile().toPath(), REPLACE_EXISTING);
                    }
                    
                    
                    
                    txtpw.print("\n" + filename.substring(0, filename.length() - 4) + "," + Integer.toHexString(id));
                    jsonpw.print(",\n");
                    jsonpw.println("        {");
                    jsonpw.println("            \"type\": \"bitmap\",");
                    jsonpw.println("            \"file\": \"minecraft:custom/" + acceptable + "\",");
                    jsonpw.println("            \"ascent\": 8,");
                    jsonpw.println("            \"height\": 8,");
                    jsonpw.println("            \"chars\": [\"\\u" + Integer.toHexString(id) + "\"]");
                    jsonpw.println("        },");
                    jsonpw.println("        {");
                    jsonpw.println("            \"type\": \"bitmap\",");
                    jsonpw.println("            \"file\": \"minecraft:custom/" + big_acceptable + "\",");
                    jsonpw.println("            \"ascent\": 24,");
                    jsonpw.println("            \"height\": 24,");
                    jsonpw.println("            \"chars\": [\"\\u" + Integer.toHexString(id + 1) + "\"]");
                    jsonpw.print("        }");
                    target = new File("custom/" + acceptable);
                    
                    Files.copy(children[i].getAbsoluteFile().toPath(), target.getAbsoluteFile().toPath(), REPLACE_EXISTING);
                    
                    id += step;
                }
            }
            
            jsonpw.print("\n");
            jsonpw.println("    ]");
            jsonpw.print("}");
            
            jsonpw.close();
            txtpw.close();
        } catch (Exception e){
            JOptionPane.showMessageDialog(null, "An error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }
    
    static String fix(String input){
        String temp = "";
        String lower = input.toLowerCase();
        for(int i = 0; i < lower.length() - 4; i++){
            char curr = lower.charAt(i);
            switch (curr) {
                case '0': 
                    temp = temp.concat("zero");
                    break;
                case '1': 
                    temp = temp.concat("one");
                    break;
                case '2': 
                    temp = temp.concat("two");
                    break;
                case '3': 
                    temp = temp.concat("three");
                    break;
                case '4':
                    temp = temp.concat("four");
                    break;
                case '5':
                    temp = temp.concat("five");
                    break;
                case '6':
                    temp = temp.concat("six");
                    break;
                case '7':
                    temp = temp.concat("seven");
                    break;
                case '8':
                    temp = temp.concat("eight");
                    break;
                case '9':
                    temp = temp.concat("nine");
                    break;
                case '_':
                    temp = temp.concat("ndrscr");
                    break;
                default: 
                    break;

            }
            if(curr >= 'a' && curr <= 'z'){
                temp = temp + curr;
            }
        }
        temp = temp.concat(lower.substring(lower.length() - 4));
        return temp;
    }
    
}
