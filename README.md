***********************************************************************************************************************************************
INTRODUCTION

This program is intended to go along with the MCEmote plugin (https://gitlab.com/jraam1/mcemote) for Minecraft Spigot servers. This program automates many of the tedious aspects of adding and maintaining private use unicode characters for both the plugin and minecraft resource packs.

***********************************************************************************************************************************************
USE 

Drag the EmoteWrangler .jar file (https://gitlab.com/jraam1/emotewrangler/-/blob/master/dist/EmoteWrangler.jar) into a file folder containing all of the images that you wish to become emotes. Please note: the filenames of the image files will be the text that gets replaced by the emote in chat. Running the .jar will prompt the user for a starting unicode code point after which the emotes are to be assigned. This program will create an EMOTES.txt file (which should be moved to the same folder as the spigot server jar), a default.json file (which should be moved to assets/minecraft/font/ in a resource pack), and a /custom folder containing appropriately named image files for use in a minecraft resource pack (this folder should be moved to assets/minecraft/textures in your resource pack). 

Files whose name follow the pattern: "*_big.*" will not be included as emotes, but the large version of an emote will point to this file if there is a file in the directory that has the same filename as the string that precedes "_big". (Kappa_big.png will be the high-resolution image file if there is a Kappa.png in the directory.) If there is no 'big' version available, the same image will be used for both the small and large size.

You may also consult: https://gitlab.com/jraam1/mcemote/-/blob/master/ManagingEmotes.m4v

***********************************************************************************************************************************************
KNOWN ISSUES

The .jar file is built for java 7 using JDK14 meaning that it _should_, but may not play nice with java 7. I recommend using the latest version of java or re-building the jar from the source if updating java is not possible.

2021-06-17
